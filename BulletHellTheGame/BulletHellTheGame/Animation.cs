﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BulletHellTheGame
{
    class Animation
    {
        private float fps;
        private Vector2 offset;
        private Rectangle[] rectangles;
        private int myFrames;

        public float FPS
        {
            get { return fps; }
        }

        public Vector2 Offset
        {
            get { return offset; }
        }

        public Rectangle[] Rectangles
        {
            get { return rectangles; }
        }

        public int MyFrames
        {
            get { return myFrames; }
            set { myFrames = value; }
        }

        public Animation(int frames, int yPos, int xStartFrame, int width, int height, float fps, Vector2 offset)
        {
            rectangles = new Rectangle[frames];
            this.fps = fps;
            this.offset = offset;
            myFrames = frames;

            for (int i = 0; i < frames; i++)
            {
                rectangles[i] = new Rectangle((i + xStartFrame) * width, yPos, width, height);
            }
        }
    }
}
