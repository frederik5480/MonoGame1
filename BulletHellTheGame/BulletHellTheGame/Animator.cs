﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace BulletHellTheGame
{
    class Animator : Component, IUpdateable
    {
        private SpriteRenderer spriteRenderer;
        private int currentIndex;
        private float timeElapsed;
        private float fps;
        private Rectangle[] rectangles;
        private string animationName;
        private Dictionary<string, Animation> animations; 

        public Animator(GameObject gameObject) : base(gameObject)
        {
            fps = 5;
            this.spriteRenderer = (SpriteRenderer) gameObject.GetComponent("SpriteRenderer");
            animations = new Dictionary<string, Animation>();
        }

        public Dictionary<string, Animation> Animations
        {
            get { return animations; }
            set { animations = value; }
        }

        public string AnimationName
        {
            get { return animationName; }
            set { animationName = value; }
        }

        public int CurrentIndex
        {
            get { return currentIndex; }
            set { currentIndex = value; }
        }

        public void Update()
        {
            timeElapsed += GameWorld.DeltaTime;

            currentIndex = (int) (timeElapsed*fps);

            if (currentIndex > rectangles.Length - 1)
            {
                timeElapsed = 0;
                currentIndex = 0;
                GameObject.OnAnimationDone(animationName);
            }

            spriteRenderer.Rectangle = rectangles[currentIndex];
        }

        public void CreateAnimation(string name, Animation animation)
        {
            animations.Add(name, animation);
        }

        public void PlayAnimation(string animationName)
        {
            if (this.animationName != animationName)
            {
                this.rectangles = animations[animationName].Rectangles;

                this.spriteRenderer.Rectangle = rectangles[0];

                this.spriteRenderer.Offset = animations[animationName].Offset;

                this.animationName = animationName;

                this.fps = animations[animationName].FPS;

                timeElapsed = 0;

                currentIndex = 0;
            }
        }
    }
}
