﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BulletHellTheGame
{
    class Collider : Component, IDrawable, ILoadable, IUpdateable
    {
        private SpriteRenderer spriteRenderer;
        private Texture2D texture2D;
        private bool doCollisionCheck;
        private List<Collider> otherColliders;
        private Animator animator;
        private Dictionary<string, Color[][]> pixels;
        private ColliderType colliderType;
        private string tag;

        public Rectangle CollisionBox
        {
            get
            {
                return new Rectangle(
                    (int)(GameObject.Transform.Position.X + spriteRenderer.Offset.X),
                    (int)(GameObject.Transform.Position.Y + spriteRenderer.Offset.Y),
                    spriteRenderer.Rectangle.Width,
                    spriteRenderer.Rectangle.Height);
            }
        }

        private Color[] CurrentPixels
        {
            get
            {              
                return pixels[animator.AnimationName][animator.CurrentIndex];
            }
        }

        public bool DoCollisionCheck
        {
            get { return doCollisionCheck; }
            set { doCollisionCheck = value; }
        }

        public Collider(GameObject gameObject, ColliderType colliderType, string tag) : base(gameObject)
        {
            this.colliderType = colliderType;
            GameWorld.Instance.Colliders.Add(this);
            otherColliders = new List<Collider>();
            doCollisionCheck = true;
            pixels = new Dictionary<string, Color[][]>();
            this.tag = tag;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Rectangle topLine = new Rectangle(CollisionBox.X, CollisionBox.Y, CollisionBox.Width, 1);
            Rectangle bottomLine = new Rectangle(CollisionBox.X, CollisionBox.Y + CollisionBox.Height, CollisionBox.Width, 1);
            Rectangle rightLine = new Rectangle(CollisionBox.X + CollisionBox.Width, CollisionBox.Y, 1, CollisionBox.Height);
            Rectangle leftLine = new Rectangle(CollisionBox.X, CollisionBox.Y, 1, CollisionBox.Height);
            spriteBatch.Draw(texture2D, topLine, null, Color.AntiqueWhite, 0, Vector2.Zero, SpriteEffects.None, 1);
            spriteBatch.Draw(texture2D, bottomLine, null, Color.AntiqueWhite, 0, Vector2.Zero, SpriteEffects.None, 1);
            spriteBatch.Draw(texture2D, rightLine, null, Color.AntiqueWhite, 0, Vector2.Zero, SpriteEffects.None, 1);
            spriteBatch.Draw(texture2D, leftLine, null, Color.AntiqueWhite, 0, Vector2.Zero, SpriteEffects.None, 1);
        }

        public void LoadContent(ContentManager content)
        {
            spriteRenderer = (SpriteRenderer) GameObject.GetComponent("SpriteRenderer");
            texture2D = content.Load<Texture2D>("CollisionTexture");
            animator = (Animator)GameObject.GetComponent("Animator");
            CachePixels();
        }

        public void Update()
        {
            CheckCollision();
        }

        private void CheckCollision()
        {
            if (doCollisionCheck)
            {
                foreach (Collider other in GameWorld.Instance.Colliders)
                {
                    if (other != this && other.tag != tag)
                    {
                        if (CollisionBox.Intersects(other.CollisionBox))
                        {
                            switch (colliderType)
                            {
                                case ColliderType.PixelCollider:
                                    if (CheckPixelCollisions(other))
                                    {
                                        if (!otherColliders.Contains(other))
                                        {
                                            GameObject.OnCollisionEnter(other);
                                            otherColliders.Add(other);
                                        }

                                        GameObject.OnCollisionStay(other);
                                    }
                                    else
                                    {
                                        if (otherColliders.Contains(other))
                                        {
                                            otherColliders.Remove(other);
                                            GameObject.OnCollisionExit(other);
                                        }
                                    }
                                    break;
                                case ColliderType.BoxCollider:
                                    if (!otherColliders.Contains(other))
                                    {
                                        GameObject.OnCollisionEnter(other);
                                        otherColliders.Add(other);
                                    }

                                    GameObject.OnCollisionStay(other);
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                        }
                        else
                        {
                            if (otherColliders.Contains(other) && colliderType != ColliderType.PixelCollider)
                            {
                                otherColliders.Remove(other);
                                GameObject.OnCollisionExit(other);
                            }
                        }
                    }
                }
            }
        }

        private void CachePixels()
        {
            foreach (KeyValuePair<string, Animation> pair in animator.Animations)
            {
                Animation animation = pair.Value;
                Color[][] colors = new Color[animation.MyFrames][];
                for (int i = 0; i < animation.MyFrames; i++)
                {
                    colors[i] = new Color[animation.Rectangles[i].Width*animation.Rectangles[i].Height];
                    spriteRenderer.Sprite.GetData(0, animation.Rectangles[i], colors[i], 0, animation.Rectangles[i].Width*animation.Rectangles[i].Height);
                }
                pixels.Add(pair.Key, colors);
            }
        }

        private bool CheckPixelCollisions(Collider other)
        {
            int top = Math.Max(CollisionBox.Top, other.CollisionBox.Top);
            int bottom = Math.Min(CollisionBox.Bottom, other.CollisionBox.Bottom);
            int left = Math.Max(CollisionBox.Left, other.CollisionBox.Left);
            int right = Math.Min(CollisionBox.Right, other.CollisionBox.Right);
            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    int firstIndex = (x - CollisionBox.Left) + (y - CollisionBox.Top)*CollisionBox.Width;
                    int secondIndex = (x - other.CollisionBox.Left) + (y - other.CollisionBox.Top)*other.CollisionBox.Width;
                    //Get the color of both pixels at this point
                    Color colorA = CurrentPixels[firstIndex];
                    Color colorB = other.CurrentPixels[secondIndex];
                    // If both pixels are not completely transparent
                    if (colorA.A != 0 && colorB.A != 0)
                    {
                        //Then an intersection has been found
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
