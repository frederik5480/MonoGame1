﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BulletHellTheGame
{
    class GameObject : Component
    {
        private List<Component> components;
        private string tag;
        private bool isLoaded;
        private Color color;

        public GameObject()
        {
            components = new List<Component>();
            this.Transform = new Transform(this, new Vector2(200, 200));
            AddComponent(Transform);
            color = Color.White;
        }

        public Transform Transform { get; }

        public string Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public void AddComponent(Component component)
        {
            components.Add(component);
        }

        public Component GetComponent(string component)
        {
            return components.Find(x => x.GetType().Name == component);
        }

        public void RemoveComponent(string component)
        {
            Component tmp = components.Find(x => x.GetType().Name == component);
            components.Remove(tmp);
        }

        public void LoadContent(ContentManager content)
        {
            if (!isLoaded)
            {
                foreach (Component component in components)
                {
                    if (component is ILoadable)
                    {
                        (component as ILoadable).LoadContent(content);
                    }
                }
                isLoaded = true;
            }
        }

        public void Update()
        {
            if (components != null)
            foreach (Component component in components)
            {
                if (component is IUpdateable)
                {
                    (component as IUpdateable).Update();
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (components != null)
            foreach (Component component in components)
            {
                if (component is IDrawable)
                {
                    (component as IDrawable).Draw(spriteBatch);
                }
            }
        }

        public void OnAnimationDone(string animationName)
        {
            if (components != null)
            foreach (Component component in components)
            {
                if (component is IAnimateable)
                {
                    (component as IAnimateable).OnAnimationDone(animationName);
                }
            }
        }

        public void CleanUp()
        {
            components = null;
            tag = null;
            isLoaded = false;
        }

        public void OnCollisionStay(Collider other)
        {
            if (components != null)
                foreach (Component component in components)
                {
                    if (component is ICollisionStay)
                    {
                        (component as ICollisionStay).OnCollisionStay(other);
                    }
                }
        }

        public void OnCollisionEnter(Collider other)
        {
            if (components != null)
                foreach (Component component in components)
                {
                    if (component is IOnCollisionEnter)
                    {
                        (component as IOnCollisionEnter).OnCollisionEnter(other);
                    }
                }
        }

        public void OnCollisionExit(Collider other)
        {
            if (components != null)
                foreach (Component component in components)
                {
                    if (component is IOnCollisionExit)
                    {
                        (component as IOnCollisionExit).OnCollisionExit(other);
                    }
                }
        }
    }
}
