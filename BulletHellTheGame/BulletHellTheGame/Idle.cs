﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulletHellTheGame
{
    class Idle : IStrategy
    {
        private Animator animator;

        public Idle(Animator animator)
        {
            this.animator = animator;
        }

        public void Update(ref Direction direction)
        {
            animator.PlayAnimation("Idle" + direction);
        }
    }
}
