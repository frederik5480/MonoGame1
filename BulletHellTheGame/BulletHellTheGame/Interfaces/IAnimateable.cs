﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulletHellTheGame
{
    interface IAnimateable
    {
        void OnAnimationDone(string animationName);
    }
}
