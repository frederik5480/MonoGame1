﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BulletHellTheGame
{
    interface IBuilder
    {
        GameObject GetResult();
        void BuildGameObject(Vector2 position);
    }
}
