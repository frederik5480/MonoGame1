﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulletHellTheGame
{
    interface ICollisionStay
    {
        void OnCollisionStay(Collider other);
    }
}
