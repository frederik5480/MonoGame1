﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulletHellTheGame
{
    interface IOnCollisionEnter
    {
        void OnCollisionEnter(Collider other);
    }
}
