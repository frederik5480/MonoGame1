﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulletHellTheGame
{
    interface IOnCollisionExit
    {
        void OnCollisionExit(Collider other);
    }
}
