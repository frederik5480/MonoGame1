﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BulletHellTheGame
{
    interface IStrategy
    {
        void Update(ref Direction direction);
    }
}
