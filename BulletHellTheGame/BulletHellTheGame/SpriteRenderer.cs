﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BulletHellTheGame
{
    class SpriteRenderer : Component, ILoadable, IDrawable, IUpdateable
    {
        private Rectangle rectangle;
        private Texture2D sprite;
        private string spritePath;
        private float layerDepth;
        private Vector2 offset;
        private Color color;

        public SpriteRenderer(GameObject gameObject, string spritePath, float layerDepth) : base(gameObject)
        {
            this.spritePath = spritePath;
            this.layerDepth = layerDepth;
            Color = Color.White;
        }

        public Rectangle Rectangle
        {
            get { return rectangle; }
            set { rectangle = value; }
        }

        public Texture2D Sprite
        {
            get { return sprite; }
        }

        public Vector2 Offset
        {
            get { return offset; }
            set { offset = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public void Update()
        {
            rectangle.Location = GameObject.Transform.Position.ToPoint();
        }

        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>(spritePath);
            rectangle = new Rectangle((int)GameObject.Transform.Position.X + (int)offset.X, (int)GameObject.Transform.Position.Y + (int)offset.Y, sprite.Width, sprite.Height);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
           spriteBatch.Draw(sprite, GameObject.Transform.Position + offset, rectangle, GameObject.Color, 0, Vector2.Zero, 1, SpriteEffects.None, layerDepth);
        }
    }
}
